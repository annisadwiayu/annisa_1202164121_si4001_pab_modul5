package com.example.annisa_1202164121_si4001_pab_modul5;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class ListArticleActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    ArticleAdapter articleAdapter;
    List<Article> articleList;
    ArticleDbHelper dbHelper;

    final String PREF_NIGHT_MODE = "NightMode";
    SharedPref mysharedpref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        mysharedpref = new SharedPref(this);

        if (mysharedpref.loadNightModeState() == true) {
            setTheme(R.style.DarkTheme);
        } else
        {
            setTheme(R.style.AppTheme);
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_article);

        dbHelper = new ArticleDbHelper(this);
        recyclerView = findViewById(R.id.rv_article);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        articleList = new ArrayList<>();

        dbHelper.readData(articleList);

        articleAdapter = new ArticleAdapter(this, articleList);
        recyclerView.setAdapter(articleAdapter);

        articleAdapter.notifyDataSetChanged();
    }
}
