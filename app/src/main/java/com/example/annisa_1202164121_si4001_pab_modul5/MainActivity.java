package com.example.annisa_1202164121_si4001_pab_modul5;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    Button btnListArticle, btnCreateArticle, btnSetting;


    final String PREF_NIGHT_MODE = "NightMode";
    SharedPreferences spNight;

    SharedPref mysharedpref;
    private ArticleDbHelper dbHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        mysharedpref = new SharedPref(this);

        if (mysharedpref.loadNightModeState() == true) {
            setTheme(R.style.DarkTheme);
        } else {
            setTheme(R.style.AppTheme);
        }

        dbHandler = new ArticleDbHelper(getApplicationContext());


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        btnCreateArticle = findViewById(R.id.btn_create_article);
        btnListArticle = findViewById(R.id.btn_list_article);
        btnSetting = findViewById(R.id.btn_setting);


        btnCreateArticle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, CreateArticleActivity.class));
            }
        });


        btnListArticle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, ListArticleActivity.class));
            }
        });

        btnSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, SettingActivity.class));
            }
        });
    }


    }
