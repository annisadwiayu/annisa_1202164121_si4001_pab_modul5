package com.example.annisa_1202164121_si4001_pab_modul5;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPref
{
    SharedPreferences mysharedpreferences;

    public SharedPref(Context context)
    {
        mysharedpreferences = context.getSharedPreferences("filename", Context.MODE_PRIVATE);
    }

    public void setNightModeState(Boolean state)
    {
        SharedPreferences.Editor editor = mysharedpreferences.edit();
        editor.putBoolean("NightMode", state);
        editor.commit();
    }

    public Boolean loadNightModeState()
    {
        Boolean state = mysharedpreferences.getBoolean("NightMode", false);
        return state;
    }

    public void setBigFontState(Boolean state)
    {
        SharedPreferences.Editor editor = mysharedpreferences.edit();
        editor.putBoolean("BigFont", state);
        editor.commit();
    }

    public Boolean loadBigFontState()
    {
        Boolean state = mysharedpreferences.getBoolean("BigFont", false);
        return state;
    }
}
