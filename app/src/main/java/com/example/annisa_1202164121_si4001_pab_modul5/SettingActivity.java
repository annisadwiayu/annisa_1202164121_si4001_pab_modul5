package com.example.annisa_1202164121_si4001_pab_modul5;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;


public class SettingActivity extends AppCompatActivity {
    Button btnSave;
    Switch swNight, swFontSize;
    SharedPreferences spFont, spNight;
    SharedPref mysharedpref;
    private TextView tes;
//    private static final String PREFS_NAME = "prefs";
//    private static final String PREF_DARK_THEME = "dark_theme";
//    private static final String PREF_FONT_LARGE = "font_large";

    SharedPreferences.Editor editNight, editFont;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        mysharedpref = new SharedPref(this);
        if (mysharedpref.loadNightModeState() == true) {
            setTheme(R.style.DarkTheme);
        }
        else if(mysharedpref.loadBigFontState() == true)
        {
            setTheme(R.style.AppTheme_FontLarge);
        }
        else {
            setTheme(R.style.AppTheme);
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);

         tes = findViewById(R.id.textView3);

        if (mysharedpref.loadBigFontState() == true) {
            tes.setTextSize(50);
        } else
        {
            tes.setTextSize(32);
        }

        swNight = findViewById(R.id.sw_night_mode);
        if (mysharedpref.loadNightModeState()) {
            swNight.setChecked(true);
        } else {
            swNight.setChecked(false);
        }

        swFontSize = findViewById(R.id.sw_font_size);
        if (mysharedpref.loadBigFontState()) {
            swFontSize.setChecked(true);
        } else {
            swFontSize.setChecked(false);
        }

        CompoundButton.OnCheckedChangeListener multiListener = new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton v, boolean isChecked) {
                switch (v.getId()) {
                    case R.id.sw_night_mode:
                        if (isChecked) {

                            mysharedpref.setNightModeState(true);
                            startActivity(new Intent(getApplicationContext(), SettingActivity.class));
                            finish();
                        } else {

                            mysharedpref.setNightModeState(false);
                            startActivity(new Intent(getApplicationContext(), SettingActivity.class));
                            finish();
                        }
                        break;
                    case R.id.sw_font_size:
                        if (isChecked) {

                            mysharedpref.setBigFontState(true);
                            startActivity(new Intent(getApplicationContext(), SettingActivity.class));
                            finish();
                        } else {

                            mysharedpref.setBigFontState(false);
                            startActivity(new Intent(getApplicationContext(), SettingActivity.class));
                            finish();
                        }
                        break;
                }
            }
        };

        swNight.setOnCheckedChangeListener(multiListener);
        swFontSize.setOnCheckedChangeListener(multiListener);

    }
        public void setting_save(View view) {
            startActivity(new Intent(SettingActivity.this, MainActivity.class));
        }
    }
